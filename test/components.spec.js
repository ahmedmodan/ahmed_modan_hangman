import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import CircleButton from './../src/components/CircleButton/CircleButton';
import ButtonGrid from './../src/components/ButtonGrid/ButtonGrid';
import FullWidthCardPanel from './../src/components/FullWidthCardPanel/FullWidthCardPanel';
import LivesChip from './../src/components/LivesChip/LivesChip';
import MenuButton from './../src/components/MenuButton/MenuButton';
import sinon from 'sinon';

describe('Components', () => {
  describe('CircleButton', () => {
    const onClick = sinon.spy();
    const disableTrue = true;
    const shallowButton = (text, color, handleClick, disable) => shallow(
        <CircleButton text={ text }
          color={ color }
          handleClick={ handleClick }
          disable={ disable }
          size="btn-large"
        />
      );
    it('should contain an anchor tag that simulates click events', () => {
      const shallowbtn = shallowButton('blue', 'blue', onClick, disableTrue);
      shallowbtn.find('a').simulate('click');
      expect(shallowbtn.find('a')).to.have.length(1);
      expect(onClick.calledOnce).to.equal(true);
    });

    it('should render text passed in as a props', () => {
      const testText = 'this is the text';
      const shallowbtn = shallowButton(testText, 'blue', onClick, disableTrue);
      expect(shallowbtn.text()).to.deep.equal(testText);
    });

    it('should change class names based on the prop passed in', () => {
      const blue = shallowButton('a', 'blue', onClick, disableTrue);
      const red = shallowButton('a', 'red', onClick, disableTrue);
      /* eslint-disable no-unused-expressions*/
      expect(blue.props().className.includes('blue')).to.be.true;
      expect(blue.props().className.includes('red')).to.be.false;
      blue.setProps({ color: 'red' });
      red.setProps({ color: 'blue' });
      expect(red.props().className.includes('red')).to.be.false;
      expect(red.props().className.includes('blue')).to.be.true;
      /* eslint-enable no-unused-expressions*/
    });
  });

  describe('ButtonGrid', () => {
    const onClick = sinon.spy();
    const shallowGrid = shallow(
      <ButtonGrid toPlay={ '1234'.split('') } played={ [1, 2] } handleClick={ onClick } />
      );
    it('should contain CircleButton Components equal to the length of the toPlay prop', () => {
      expect(shallowGrid.find(CircleButton)).to.have.length(4);
      shallowGrid.setProps({ toPlay: '123456'.split('') });
      expect(shallowGrid.find(CircleButton)).to.have.length(6);
    });

    it('should render each CircleButton wrapped with a div', () => {
      expect(shallowGrid.find(CircleButton).at(0).parent().is('div')).to.equal(true);
      expect(shallowGrid.find(CircleButton).at(1).parent().is('div')).to.equal(true);
      expect(shallowGrid.find(CircleButton).at(2).parent().is('div')).to.equal(true);
    });

    it('should wrap all of the buttons with a single div of class name: row', () => {
      expect(shallowGrid.find('.row')).to.have.length(1);
    });
  });

  describe('FullWidthCardPanel', () => {
    const content = 'this is some content';
    const content2 = 'different content';
    const shallowPanel = shallow(<FullWidthCardPanel content={ content } />);
    it('should contain two divs an an h2', () => {
      expect(shallowPanel.find('h2')).to.have.length(1);
      expect(shallowPanel.find('div')).to.have.length(2);
      expect(shallowPanel.find('div').at(0).hasClass('card-panel')).to.equal(true);
      expect(shallowPanel.find('div').at(1).hasClass('card-content')).to.equal(true);
    });

    it('should render content passed into it as props', () => {
      expect(shallowPanel.find('h2').text()).to.deep.equal(content);
      shallowPanel.setProps({ content: content2 });
      expect(shallowPanel.find('h2').text()).to.deep.equal(content2);
    });
  });

  describe('LivesChip', () => {
    const shallowChip = shallow(<LivesChip numOfLives={ 5 } color="blue" />);
    it('should contain two divs', () => {
      expect(shallowChip.find('div')).to.have.length(2);
    });

    it('should display change the display text based on the num passed in via props', () => {
      expect(shallowChip.text()).to.deep.equal('5 lives left.');
      shallowChip.setProps({ numOfLives: 2 });
      expect(shallowChip.text()).to.deep.equal('2 lives left.');
    });

    it('should change the phrase when there is 1 life left', () => {
      expect(shallowChip.text()).to.deep.equal('2 lives left.');
      shallowChip.setProps({ numOfLives: 1 });
      expect(shallowChip.text()).to.deep.equal('1 life left.');
    });

    it('should not display less than 0 lives left', () => {
      expect(shallowChip.text()).to.deep.equal('1 life left.');
      shallowChip.setProps({ numOfLives: -1 });
      expect(shallowChip.text()).to.deep.equal('0 lives left.');
    });
  });

  describe('MenuButton', () => {
    const handleRenew = sinon.spy();
    const handleSwitch = sinon.spy();
    const shallowMenu = shallow(
      <MenuButton handleRenew={ handleRenew } handleSwitch={ handleSwitch } />
      );
    it('should one ul with two li tags wrapped in a div', () => {
      expect(shallowMenu.find('li')).to.have.length(2);
      expect(shallowMenu.find('ul')).to.have.length(1);
      expect(shallowMenu.find('ul').parent().is('div')).to.equal(true);
    });

    it('should simlate clicks with both buttons', () => {
      const mountedMenu = mount(
        <MenuButton handleRenew={ handleRenew } handleSwitch={ handleSwitch } />
      )
      mountedMenu.find('a').at(1).simulate('click');
      /* eslint-disable no-unused-expressions*/
      expect(handleRenew.calledOnce).to.be.true;
      expect(handleSwitch.calledOnce).to.be.false;
      mountedMenu.find('a').at(2).simulate('click');
      expect(handleSwitch.calledOnce).to.be.true;
      /* eslint-enable no-unused-expressions*/
    });
  });
});


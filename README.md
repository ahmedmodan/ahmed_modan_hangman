# Hangman
A coding challenge for Coursera

## To Play
To play the game enter in your email at the home page. Once you have entered your email
you will be redirected to the play page. You must enter your email to play the game.
In order to guess the phrase simply press the buttons to guess those letters or if
you are on a computer with a keyboard attached use the keyboard to guess.
Currently the game state is stored in local storage. this can be refactored to
store in a database later.

## Getting Started
In order to get started make sure you have Node > 5.6 installed.
Node 6 will unfortunately break some dependencies. So your version must be
below 6 as well.

In order to run the application with the production ready bundle that I have
already generated execute the following commands:
```shell
  npm install --production
  npm run server-prod
```
>The above commands will not install devDependencies. If you would
like to run the tests or develop you must execute the command below and
install devDependencies.

To install devDependencies as well as the regular dependencies execute
the following command:
```shell
  npm install
```

After you have installed all of the dependencies you can
run the tests by executing the following commands:
```shell
  # to run tests once:
  npm test
  # to run tests in watch mode while writing tests
  npm run test:dev
```

If you would like to develop, you can run the application
in development mode by executing the following command:
```shell
  npm run dev
```
>Development mode will update the client using webpack HMR allowing you to
update your components in real time without losing state. It will also lint
on every rebuild.

Once you are finished developing and would like to build your code and
start your application simply execute the following command:
```shell
  npm start
```
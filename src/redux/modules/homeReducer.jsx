
export const homeState = {
  gameKey: '',
  emailInput: '',
  phrase: '',
  state: '',
  numTriesLeft: 0,
  alphabet: 'abcdefghijklmnopqrstuvwxyz'.split(''),
  played: [],
  newUser: false
};

export const GET_GAME_STATE = 'GET_GAME_STATE';
export const SET_STATE_TO_LOCAL_STORAGE = 'SET_STATE_TO_LOCAL_STORAGE';
export const UPDATE_EMAIL_INPUT = 'UPDATE_EMAIL_INPUT';
export const SAVE_RESPONSE = 'SAVE_RESPONSE';
export const GUESS_LETTER = 'GUESS_LETTER';
export const CHANGE_USER = 'CHANGE_USER';
export const REMOVE_CACHE = 'REMOVE_CACHE';
export const courseraAPI = 'http://hangman.coursera.org/hangman/game';

export function saveResponse({ game_key, phrase, state, num_tries_left }, played) {
  const payload = {
    gameKey: game_key,
    numTriesLeft: Number(num_tries_left),
    phrase,
    state,
    played
  };
  return { type: SAVE_RESPONSE,
           payload };
}

export function removeCache() {
  return { type: SAVE_RESPONSE,
           payload: homeState };
}

export function changeUser(payload = false) {
  return { type: CHANGE_USER,
           payload };
}

export function guessLetter(letter, key, alreadyPlayed) {
  const data = JSON.stringify({ guess: letter });

  return dispatch => fetch(`${courseraAPI}/${key}`, {
    method: 'POST',
    body: data
  }).then(response => response.json())
    .then(result => dispatch(saveResponse(result, alreadyPlayed)));
}

export function updateEmailInput(payload) {
  return { type: UPDATE_EMAIL_INPUT,
           payload };
}

export function getGameState() {
  return { type: GET_GAME_STATE,
           payload: JSON.parse(localStorage.getItem('gameState')) };
}

export function setStateToLocalStorage() {
  return { type: SET_STATE_TO_LOCAL_STORAGE };
}


export function submitEmailToCoursera(payload) {
  const data = JSON.stringify({ email: payload });

  return dispatch => fetch(courseraAPI, {
    method: 'POST',
    body: data
  }).then(response => response.json())
    .then(result => dispatch(saveResponse(result, [])));
}

export const actions = {
  getGameState,
  setStateToLocalStorage,
  updateEmailInput,
  submitEmailToCoursera,
  guessLetter,
  changeUser,
  removeCache
};

export default function homeReducer(state = homeState, action) {
  switch (action.type) {
    case GET_GAME_STATE:
      return Object.assign({}, state, action.payload);
    case SAVE_RESPONSE:
      return Object.assign({}, state, action.payload);
    case UPDATE_EMAIL_INPUT:
      return Object.assign({}, state, { emailInput: action.payload });
    case CHANGE_USER:
      return Object.assign({}, state, { newUser: action.payload });
    case SET_STATE_TO_LOCAL_STORAGE:
      localStorage.setItem('gameState', JSON.stringify(state));
      return state;
    default:
      return state;
  }
}

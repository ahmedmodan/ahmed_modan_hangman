import React, { PropTypes } from 'react';
import NavBar from './../components/NavBar/NavBar';


function CoreLayout({ children }) {
  return (
    <div>
      <NavBar title="Hangman" />
      <div className="container" >
        {children}
      </div>
    </div>
  );
}

CoreLayout.propTypes = {
  children: PropTypes.element
};

export default CoreLayout;

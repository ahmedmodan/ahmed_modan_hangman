import React, { PropTypes } from 'react';
import Icon from './../Icon/Icon';
import CircleButton from './../CircleButton/CircleButton';

require('./MenuButton.scss');

export default function MenuButton({ handleRenew, handleSwitch }) {
  return (
    <div className="fixed-action-btn bottom-right click-to-toggle horizontal">
      <CircleButton text={ <Icon type="menu" /> }
        color="blue-grey darken-2"
        disable={ false }
        size="btn-large"
      />
      <ul>
        <li>
          <CircleButton text={ <Icon type="autorenew" /> }
            color="blue-grey"
            disable={ false }
            handleClick={ handleRenew }
          />
          <small className="buttonLabel">New Game</small>
        </li>
        <li>
          <CircleButton text={ <Icon type="account_circle" /> }
            color="blue-grey"
            disable={ false }
            handleClick={ handleSwitch }
          />
          <small className="buttonLabel">Change User</small>
        </li>
      </ul>
    </div>
  );
}

MenuButton.propTypes = {
  handleRenew: PropTypes.func.isRequired,
  handleSwitch: PropTypes.func.isRequired,
};

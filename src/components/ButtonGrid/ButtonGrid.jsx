import React, { PropTypes } from 'react';
import CircleButton from './../CircleButton/CircleButton';

export default function ButtonGrid({ toPlay, played, handleClick }) {
  const letters = toPlay.map((letter, i) => {
    let disabled = false;
    if (played.includes(letter)) disabled = true;
    return (
      <div key={ i } className="col s3 m2 l1">
        <CircleButton
          text={ letter }
          disable={ disabled }
          handleClick={ handleClick }
          color="blue-grey"
          size="btn-large"
        />
      </div>
    );
  });
  return (
    <div className="row">
      { letters }
    </div>
  );
}

ButtonGrid.propTypes = {
  toPlay: PropTypes.arrayOf(PropTypes.string).isRequired,
  played: PropTypes.array.isRequired,
  handleClick: PropTypes.func.isRequired,
};

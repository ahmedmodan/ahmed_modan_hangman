import React, { PropTypes } from 'react';

require('./FullWidthCardPanel.scss');

export default function FullWidthCardPanel({ content }) {
  return (
    <div className="card-panel">
      <div className="card-content">
        <h2 className="phrase">{ content }</h2>
      </div>
    </div>
  );
}

FullWidthCardPanel.propTypes = {
  content: PropTypes.string.isRequired,
};

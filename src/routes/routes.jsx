import React from 'react';
import { Route, IndexRoute, Redirect } from 'react-router';

import CoreLayout from './../layouts/CoreLayout';
import Home from './../containers/Home/Home';
import Play from './../containers/Play/Play';


export default (
  <Route path="/" component={CoreLayout}>
    <IndexRoute component={Home} />
    <Route path="play" component={Play} />
    <Redirect from="*" to="/" />
  </Route>
);
